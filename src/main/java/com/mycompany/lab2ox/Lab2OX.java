/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2ox;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Lab2OX {

    static char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'},};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();
        while(true) {
            printBoard();
            printTurn();
            inputNumber();
            if(isWin()){
                printBoard();
                printWinner();
                break;
            }
            switchPlayer();
        }
        
    
    }

    private static void printWelcome() {
        System.out.println("Welcome to OX !!!");
    }

    private static void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println("Player " + currentPlayer + " turn");
    }

    private static void inputNumber() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Enter a number to place " + currentPlayer + ": ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (board[row - 1][col - 1] == '-') {
                board[row - 1][col - 1] = currentPlayer;
                return;
            }
        }
    }

    private static void switchPlayer() {
        // currentPlayer = currentPlayer=='X'?'O':'X';
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }
    
    private static boolean isWin() {
        if(checkRow()|checkCol()|checkDiagonal()) {
            return true;
        }
        return false;
    }

    private static void printWinner() {
        System.out.println("Congratulations! Player "+currentPlayer+" Won!!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == currentPlayer && board[i][1] == currentPlayer && board[i][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol() {
         for (int i = 0; i < 3; i++) {
            if (board[0][i] == currentPlayer && board[1][i] == currentPlayer && board[2][i] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiagonal() {
        return (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) ||
                (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer);
    }
    
}
